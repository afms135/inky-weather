# Raspberry Pi Weather Display

A weather forecast display using a Raspberry Pi Zero and a Pimoroni Inky pHAT.

Shows the date, current weather, temperature, humidity, wind speed and direction, UV level and probability of rain.

![pic](pic.jpg)

## Instructions

Requires the [Inky](https://github.com/pimoroni/inky) and [PIL/Pillow](https://github.com/python-pillow/Pillow) python libraries installed.

Obtain an DataPoint API key from the [Met Office](https://www.metoffice.gov.uk/services/data/datapoint) and and insert it into the `API_KEY` variable.

Find the numeric code for your location and insert it into the `LOC_CODE` variable.

Run the `main.py` script to update the display with the current forecast.

## systemd

Example systemd service and timer files can be found the the `systemd` directory designed to be installed in `/etc/systemd/system`.

When enabled this runs the `main.py` script at the beginning of every hour.

## Credits

Icons used are [Weather Icons](https://erikflowers.github.io/weather-icons/) converted to PNG.

Font used is the 8x16 [Terminus](http://terminus-font.sourceforge.net/) font converted to PIL.

