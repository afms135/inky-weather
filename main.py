#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import json
from urllib.request import urlopen
from datetime import datetime
from PIL import Image, ImageDraw, ImageFont
from inky import InkyPHAT

API_KEY = ''
LOC_CODE = '310009' #Glasgow
#LOC_CODE = '351351' #Edinburgh

#
# Get weather data
#
URL = 'http://datapoint.metoffice.gov.uk/public/data/val/wxfcs/all/json/' + LOC_CODE + '?res=3hourly&key=' + API_KEY
con = urlopen(URL, timeout=10)
resp = json.load(con)
con.close()

mins = datetime.now().hour * 60
day = resp['SiteRep']['DV']['Location']['Period'][0]['Rep']
for i in range(0, len(day)):
	data = day[i]
	if int(data['$']) >= mins: # Choose correct forecast
		break

weather     = int(data['W'])
temperature = int(data['T'])
humidity    = int(data['H'])
wind        = int(data['S'])
wind_dir    = data['D']
UV          = int(data['U'])
rain        = int(data['Pp'])
visibility  = data['V']

if UV >= 0 and UV <= 2:
	UV = 'LOW'
elif UV >= 3 and UV <= 5:
	UV = 'MED'
elif UV >= 6 and UV <= 7:
	UV = 'HIGH'
elif UV >= 8 and UV <= 10:
	UV = 'VHIH'
else:
	UV = 'EXTR'

print('    Weather Type:', weather)
print('-----------------')
print('     Temperature:', temperature)
print('        Humidity:', humidity)
print('        UV Index:', UV)
print('Rain Probability:', rain)
print('      Wind Speed:', wind)
print('        Wind Dir:', wind_dir)
print('      Visibility:', visibility)

#
# Draw on background
#
img = Image.open('background.png')
icon = Image.open('./icons/' + str(weather) + '.png')
fnt = ImageFont.load('./font/ter-u16b.pil')
draw = ImageDraw.Draw(img)
img.paste(icon, box=(2,18)) # Weather icon

# Print data
def prnt_x(x, y, string): # Print centred string on x axis
	d_x = x - fnt.getsize(string)[0] / 2
	draw.text((d_x, y), string, font=fnt, fill=1)

prnt_x(106, 0, datetime.now().strftime('%A %d %b %Y'))
prnt_x(133, 24, str(temperature) + 'C')
prnt_x(194, 24, str(humidity) + '%')
prnt_x(133, 53, str(wind))
prnt_x(194, 53, wind_dir)
prnt_x(133, 82, UV)
prnt_x(194, 82, str(rain) + '%')

#
# Draw on display
#
inky = InkyPHAT('red')
inky.set_image(img)
inky.set_border(inky.WHITE)
inky.show()

